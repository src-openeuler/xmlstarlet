Name:           xmlstarlet
Version:        1.6.1
Release:        13
Summary:        Command Line Tool to Process XML Documents
License:        MIT
URL:            http://xmlstar.sourceforge.net/
Source0:        http://downloads.sourceforge.net/xmlstar/%{name}-%{version}.tar.gz
Patch0001:      xmlstarlet-1.6.1-nogit.patch

BuildRequires:  xmlto automake autoconf libxslt-devel libxml2-devel >= 2.6.23 gcc

%description
XMLStarlet (xml) is a command line XML toolkit which can be used to
transform, query, validate, and edit XML documents and files using simple
set of shell commands in similar way it is done for plain text files using
'grep', 'sed', 'awk', 'tr', 'diff', or 'patch'.

%package help
Summary:    Help documentation for %{name}

%description help
Man pages and other related help documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -i
%configure --disable-static-libs --with-libxml-include-prefix=%{_includedir}/libxml2 \
           --docdir=%{_pkgdocdir}
%make_build

%install
%make_install
mv $RPM_BUILD_ROOT%{_bindir}/xml $RPM_BUILD_ROOT%{_bindir}/xmlstarlet

%check
make check

%files
%doc AUTHORS ChangeLog NEWS README Copyright TODO
%doc %{_pkgdocdir}/*
%{_bindir}/xmlstarlet

%files help
%{_mandir}/man1/xmlstarlet.1*

%changelog
* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 1.6.1-13
- Completing build dependencies to fix gcc compiler missing error

* Wed Jan 8 2020 wangzhishun <wangzhishun1@huawei.com> - 1.6.1-12
- Package init
